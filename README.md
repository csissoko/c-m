# Les étapes à suivre:

* Télécharger un éditeur de texte.
Cela nous permettra d'écrire le code de notre application.

[Sublime Texte](https://download.sublimetext.com/Sublime%20Text%20Build%203126.dmg)

Nous allons créer un site simple de présentation d'un produit laitier

Appellons le 'C&M' qui se lit C and M en anglais.

Notre application va avoir les écrans suivants :

## La page d'acceuil.

En général le fichier contenant le code de la page d'acceuil s'appelle (index.html)

Donc nous allons commencer par ce fichier.

La page d'acceuil c'est le point d'entrée de l'application. Nous devons donc pouvoir acceder à toutes les pages depuis cette page.

## Une Page pour lister les produits

## Une Page de présentation d'un produit

HTML tout seul ne donne pas de joli site, il a besoin d'une autre composante qui s'appelle CSS

Les CSS permettent de definir le style de nos pages.

# Étape 1
Création d'un page simple HTML avec une classe css.

# État 2
- Création d'un fichier `styles.css` qui contiendra tous les styles de nnos pages.  
- Remplacer la balise <styles> dans `index.html` par le fichier `styles.css`

# Étape 3
Utilisation d'une bibliothèque de styles [Bootstrap](http://getbootstrap.com/)


